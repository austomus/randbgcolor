#!/usr/bin/env python3
# Print random colors to the terminal.
#
# Copyright (C) 2021  Austomus
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import argparse
from random import choice
from colorama import init
from termcolor import cprint

colors = ["grey", "red", "green", "yellow", "blue", "magenta", "cyan", "white"]
parser = argparse.ArgumentParser(description="Print random background colors to the terminal.")
parser.add_argument("colors", nargs="*", default=colors)
args = parser.parse_args()
init()

bg_colors = []
for c in args.colors:
    if c not in colors:
        parser.error(f"'{c}' is an invalid color")
    bg_colors.append(f"on_{c}")

while True:
    try:
        cprint(" ", on_color=choice(bg_colors), end="")
    except KeyboardInterrupt:
        sys.exit()
